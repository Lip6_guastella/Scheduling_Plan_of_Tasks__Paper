%% $Id: iasart.cls,v 1.17 2003/05/08 13:10:24 peterlin Exp $
%%
%% This is file iasart.cls.
%% It is not part of the standard LaTeX distribution.
%%
%% This file is for printing papers in Image Analysis and Stereology format
%%
%% \CharacterTable
%%  {Upper-case    \A\B\C\D\E\F\G\H\I\J\K\L\M\N\O\P\Q\R\S\T\U\V\W\X\Y\Z
%%   Lower-case    \a\b\c\d\e\f\g\h\i\j\k\l\m\n\o\p\q\r\s\t\u\v\w\x\y\z
%%   Digits        \0\1\2\3\4\5\6\7\8\9
%%   Exclamation   \!     Double quote  \"     Hash (number) \#
%%   Dollar        \$     Percent       \%     Ampersand     \&
%%   Acute accent  \'     Left paren    \(     Right paren   \)
%%   Asterisk      \*     Plus          \+     Comma         \,
%%   Minus         \-     Point         \.     Solidus       \/
%%   Colon         \:     Semicolon     \;     Less than     \<
%%   Equals        \=     Greater than  \>     Question mark \?
%%   Commercial at \@     Left bracket  \[     Backslash     \\
%%   Right bracket \]     Circumflex    \^     Underscore    \_
%%   Grave accent  \`     Left brace    \{     Vertical bar  \|
%%   Right brace   \}     Tilde         \~}
\NeedsTeXFormat{LaTeX2e}[1995/12/01]
\ProvidesClass{iasart}
              [2003/04/28 v1.2 UH, PP
 Article class for Image Analysis and Stereology]

%% -------- initial code --------

\newif\if@final\@finaltrue
\newif\if@submit\@submitfalse
\newif\if@draft\@draftfalse
\newif\if@afour\@afourtrue
\newif\if@ias\@iasfalse
\newif\if@continue\@continuefalse

%% ----------- options -----------

%% ------- paper size: only a4, letter --------
%% ------- final only with a4paper! ---------

\DeclareOption{a4paper}
   {\setlength\paperheight {297mm}%
    \setlength\paperwidth  {210mm}}
\DeclareOption{letterpaper}
   {\setlength\paperheight {11in}%
    \setlength\paperwidth  {8.5in}
    \@afourfalse}

%% ------- document versions --------

\DeclareOption{submit}
    {\@submittrue\@finalfalse\@draftfalse}
\DeclareOption{final}
    {\@submitfalse\@finaltrue\@draftfalse}
\DeclareOption{ias}
    {\@iastrue}
\DeclareOption{owndraft}
    {\@submitfalse\@finalfalse\@drafttrue}
\DeclareOption*{\ClassWarning{iasart}{option `\CurrentOption' unknown}}
%\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}

%% ------- executing options ----------------

\ExecuteOptions{a4paper,submit}
\ProcessOptions

\if@draft\else\ExecuteOptions{a4paper}\fi
% allow letterpaper only for draft version
%% ------- loading packages ----------------

\if@submit%
   \LoadClass[12pt,a4paper]{article}%\RequirePackage[nomarkers,lists]{endfloat}%
   \fi
\if@final
  \LoadClass[11pt,a4paper,twoside]{article}%
  \fi%
\if@draft
  \if@afour
  \LoadClass[11pt,fleqn,a4paper]{article}
  \else
  \LoadClass[11pt,fleqn]{article}
  \fi
\fi%\RequirePackage{endfloat}\fi%


\RequirePackage{natbib}[1999/05/28] %% for references in Acta Style
\RequirePackage{times}[1999/03/29]
\RequirePackage{mathptmx}[1998/06/28]
\RequirePackage{multicol}[1999/06/18] % This has to be delayed after definition
\if@submit\RequirePackage[noheads,nolists]{endfloat}[1995/10/11]\fi
\RequirePackage{lastpage}[1994/05/29]
\RequirePackage{textcase}[1998/11/12] % For headings that contain formulae
\RequirePackage{pifont}[1999/03/29] % For typesetting \micro

%%%%%%%%%%%%%%%%%%%%%%%% Einordnen:

\if@final
\DeclareMathAlphabet{\mi}{OT1}{phv}{m}{it}  % for nice letters in headings that are set helvetica
\else
\newcommand{\mi}{}
\fi


\if@submit
\newcommand{\SubmitPageHead}{\small\scshape\protect\TheShortAuthors: %
     \itshape{\protect\TheShortTitle}}
\fi

\newsavebox{\vcbox}
\newlength{\vcheight}
\newlength{\vcskip}
\newenvironment{Vcenter}
{\begin{lrbox}{\vcbox}\begin{minipage}{\linewidth}}
{\end{minipage}\end{lrbox}\noindent%
\settoheight{\vcheight}{\usebox{\vcbox}}
\setlength{\vcskip}{\textheight}
\addtolength{\vcskip}{-\vcheight}
\setlength{\vcskip}{0.2\vcskip}
\addtolength{\vcskip}{-\topmargin}
\addtolength{\vcskip}{-1in}
%vcskip =\the\vcskip\ \    vcheight = \the\vcheight

\par
%\vspace*{\vcskip}
\vskip\vcskip
\usebox{\vcbox}
}

%%---------- Abbreviations -----------------
\newcommand{\etal}{\textit{et al.}\/}
\newcommand{\eg}{\textit{e.\/g.}\/}
\newcommand{\ie}{\textit{i.\/e.}\/}
\newcommand{\cf}{\textit{cf.}\/}
\newcommand{\vs}{\textit{vs.}\/}

\newcommand{\micro}{\mbox{\Pifont{psy}\small m}}

%%---------- Figures and Tables -----------------

\renewcommand{\figurename}{Fig.}
\renewcommand{\tablename}{Table}

\renewcommand{\@makecaption}[2]{\vspace{6pt}#1. \textit{#2}}
\if@submit
  \renewcommand{\@makecaption}[2]{\vspace{6pt}#1: \textit{#2}\par\vspace{18pt}}
\fi
\if@draft
  \renewcommand{\@makecaption}[2]{\vspace{6pt}#1: \textit{#2}\par\vspace{6pt}}
\fi


\if@submit
%% Figures and Tables sections : need to redefine commands from endfloat
\renewcommand{\processfigures}{%
 \expandafter\ifnum \csname @ef@fffopen\endcsname>0
  \immediate\closeout\efloat@postfff \ef@setct{fff}{0}
  \clearpage                                                        % bj
  \def\leftmark{\SubmitPageHead\hfill FIGURES}
  \let\rightmark\leftmark
  \processfigures@hook \@input{\jobname.fff}
 \fi
  }
\renewcommand{\processtables}{%
  \expandafter\ifnum \csname @ef@tttopen\endcsname>0
  \immediate\closeout\efloat@postttt \ef@setct{ttt}{0}
  \clearpage                                                      % bj
  \def\leftmark{\SubmitPageHead\hfill TABLES}
  \let\rightmark\leftmark
  \processtables@hook \@input{\jobname.ttt}
 \fi}

%%    indicators for "place figure (table) here"
\newcommand{\old@baseli}{}
\renewcommand{\figureplace}{%   indicators
  \renewcommand{\old@baseli}{\baselinestretch}
  \marginpar{%\baselinestretch{1}
  \renewcommand{\baselinestretch}{1}
  \parbox{25mm}{\footnotesize\slshape{$\leftarrow$ insert\ \figurename~\thepostfig}}}
  \renewcommand{\baselinestretch}{\old@baseli}
  }
\renewcommand{\tableplace}{%
  \renewcommand{\old@baseli}{\baselinestretch}
  \marginpar{%\baselinestretch{1}
  \renewcommand{\baselinestretch}{1}
  \parbox{25mm}{\footnotesize\textsl{$\leftarrow$ insert\ \tablename~\theposttbl}}}
  \renewcommand{\baselinestretch}{\old@baseli}
  }
\fi
%% -------- Information needed for IAS ---------------------

%% --- for printing "Warnings"

\newcommand{\PleaseCom}[1]{{\small(Please use \textnormal{\texttt{$\backslash$#1})}}}

% -------- Authors --------

% Usage: \author{Name}{affil-number}
\newcount\auth@rno
\auth@rno=0

\renewcommand{\author}[2][1]{
 \advance\auth@rno by 1 \relax%
 \expandafter\def\csname anAuth@r\number\auth@rno\endcsname{#2}%
 \expandafter\def\csname auth@raffil\number\auth@rno\endcsname{#1}%
}

% ------ Postal Addresses and Email --------
% Usage \affiliation{number}{postal address}
\newcounter{affil@no}
\setcounter{affil@no}{0}

\newcommand{\affiliation}[2][1]{\expandafter\def\csname an@ffil#1\endcsname{#2}\relax
 \stepcounter{affil@no}} %

\let\TheEmail\@empty
\newcommand{\email}[1]{\def\TheEmail{#1}}

% ---- Output of Authors & Affiliation. Change Style here ----
\newcommand{\PrintAuthor}[1]{{\authorsize\scshape\csname anAuth@r#1\endcsname}\upshape} %
\if@submit\renewcommand{\PrintAuthor}[1]{\csname anAuth@r#1\endcsname} \fi%
\newcommand{\PrintAuthorsAffil}[1]{\csname auth@raffil#1\endcsname} %
\newcommand{\PrintAffiliation}[1]{\csname an@ffil#1\endcsname\relax }%

% ---- printing the affiliation number
\newcommand{\@affilstar}[1]{$\mbox{}^{#1}$}  %% <- change format here


%%--------- Author, short title for heading; headers ------

\newcommand{\TheShortAuthors}{No short authors given. \PleaseCom{shortauthors}}
\newcommand{\shortauthors}[1]{\renewcommand{\TheShortAuthors}{#1}}
\newcommand{\ArticleType}{Original Research Paper}

\newcommand{\TheShortTitle}{No short title given. \PleaseCom{shorttitle}}
\newcommand{\shorttitle}[1]{\renewcommand{\TheShortTitle}{#1}}

%%---------- Abstract & Keywords ----------

\newcommand{\TheAbstract}{Here will be the abstract. \PleaseCom{abstract}}
\renewcommand{\abstract}[1]{\renewcommand{\TheAbstract}{#1}}

\newcommand{\TheKeyWords}{No key words. \PleaseCom{keywords}}
\newcommand{\keywords}[1]{\renewcommand{\TheKeyWords}{#1}}
\newcommand{\KeyWordsLabel}{Keywords:~}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%---- Information to be entered in Editorial office


%%--------- Volume, Acceptance Date, Start page

\newcommand{\TheVolume}{?? \PleaseCom{volume}}
\newcommand{\volume}[1]{\renewcommand{\TheVolume}{#1}}

\newif\if@StartPageUndef\@StartPageUndeftrue
\newcommand{\TheStartP@ge}{1}
\newcommand{\TheStartPage}{\if@StartPageUndef?? \PleaseCom{startpage}\else\TheStartP@ge\fi}
\newcounter{auxp@ge} 
   % I (Ute) 've put this outside the command \startpage to avoid redefining if \startpage is used more than once
\newcommand{\startpage}[1]{\renewcommand{\TheStartP@ge}{#1}%
    \setcounter{auxp@ge}{\TheStartP@ge}%
    \@StartPageUndeffalse}
\if@ias\relax\else\startpage{1}\fi

\newcommand{\TheAccepted}{?? \PleaseCom{accepted}}
\newcommand{\accepted}[1]{\renewcommand{\TheAccepted}{#1}}





%% newline for titles: some extra space
\newcommand{\TheTitle}{NO TITLE. \PleaseCom{title}}

\renewcommand{\title}[1]{\renewcommand{\TheTitle}{\MakeTextUppercase{#1}}}

\newcommand{\newtitleline}{\protect\vspace{2pt}\protect\newline}%{\protect\\[+1mm]}

%% -------  paper environment etc ------------

\if@final
   \newenvironment{paper}
   {\setcounter{page}{\value{auxp@ge}}
   \maketitle\par\begin{multicols}{2}\frenchspacing}
   {\end{multicols}\setcounter{auxp@ge}{\value{page}}}
   \newcommand{\flushpage}{\end{multicols}\newpage\begin{multicols}{2}}
   \newcommand{\onecol}{\end{multicols}}
   \newcommand{\twocols}{\begin{multicols}{2}}
\fi
\if@submit
   \newenvironment{paper}
%   {\maketitle\newpage\small\renewcommand{\baselinestretch}{1.67}\normalsize}
   {\maketitle\par}
   {}
  \newcommand{\flushpage}{}
  \newcommand{\onecol}{}
  \newcommand{\twocols}{}
\fi
\if@draft
   \newenvironment{paper}
   {\maketitle\par}
   {}
  \newcommand{\flushpage}{}
  \newcommand{\onecol}{}
  \newcommand{\twocols}{}
\fi

%% ------- one column figures and tables -------

\if@final
  \newlength\@oldparindent
  \renewenvironment{figure}[1][h]{%
     \def\@captype{figure}
     \setlength{\@oldparindent}{\parindent}
     \setlength{\parindent}{0pt}
  }%% do basically nothing, but prevent indenting
  {\setlength{\parindent}{\@oldparindent}} %% do basically nothing
  \renewenvironment{table}[1][h]{%
  \def\@captype{table}
       \setlength{\@oldparindent}{\parindent}
     \setlength{\parindent}{0pt}
  }%% do basically nothing, but prevent indenting
  {\setlength{\parindent}{\@oldparindent}}
\fi

%% ------- document layout -------------

%% -------- no hyphenation for IAS ------------
\if@final
  \hyphenpenalty=10000
\else
  \if@submit
     \hyphenpenalty=9999
   \else
     \hyphenpenalty=50
    \fi
\fi
%% -------- font sizes -----------

%% have to be modified for final version because of inter line spacing

% normalsize
\if@submit %% => 12pt, double spaced
  \abovedisplayskip 18pt \@plus4pt \@minus6pt
  \abovedisplayshortskip \z@ \@plus3pt
  \belowdisplayshortskip 8pt \@plus4pt \@minus4pt
  \belowdisplayskip \abovedisplayskip
  \renewcommand\normalsize{%
     \@setfontsize\normalsize{12pt}{14pt}%\@plus1pt\@minus0.5pt}%
     }
  \normalsize
%% small
  \renewcommand\small{% this should only change the font size here, not the spacing
     \@setfontsize\small{11pt}{12.5pt}%\@plus1pt\@minus0.5pt}%
      }
  \newcommand\tablesize{% for wide tables: reduce from 11pt/172 mm to 12pt/150mm
     \@setfontsize\tablesize{9.3pt}{10.5pt}%
      }
\else %% => 11pt
  \abovedisplayskip 11pt \@plus2pt \@minus6pt
  \abovedisplayshortskip \z@ \@plus2pt
  \belowdisplayshortskip 6.5pt \@plus2.5pt \@minus3pt
  \belowdisplayskip \abovedisplayskip

  \setlength{\multicolbaselineskip}{0pt\@plus0.3pt\@minus0.2pt}
  \renewcommand\normalsize{%
     \@setfontsize\normalsize{11pt}{12.5pt}%\@plus1pt\@minus0.5pt}%
     }
  \normalsize
%% small
  \renewcommand\small{% this should only change the font size here, not the spacing
     \@setfontsize\small{10pt}{12.5pt}%\@plus1pt\@minus0.5pt}%
      }
  \newcommand\tablesize{}
\fi
\if@draft
   \renewcommand\tablesize{%
   \@setfontsize\tablesize{10pt}{11.5pt}%
   }
\fi
%% ---- enumerate and itemize, with labels at left margin
\if@final
\def\@listi{\labelwidth \parindent
                 \labelsep 0pt
                 \leftmargin \parindent
                 \topsep 1pt \@plus 1pt \@minus 0.5pt
                 \parsep 3pt \@plus 1pt \@minus 0.5pt
                 \partopsep 0pt
                 \itemsep \parsep }%
\def\@listii {\setlength{\leftmargin}{5mm}
              \labelsep 0pt
              \labelwidth \leftmargin
              \topsep 1pt \@plus 1pt \@minus 0.5pt
              \parsep 3pt \@plus 1pt \@minus 0.5pt
              \partopsep 0pt
              \itemsep \parsep }%
\fi

\def\enumerate{%
  \ifnum \@enumdepth >\thr@@\@toodeep\else
    \advance\@enumdepth\@ne
    \edef\@enumctr{enum\romannumeral\the\@enumdepth}%
      \expandafter
      \list
        \csname label\@enumctr\endcsname
        {\usecounter\@enumctr\def\makelabel##1{\mbox{##1}\hfil}}%{\hss\llap{##1}}}%
  \fi}

\def\itemize{%
  \ifnum \@itemdepth >\thr@@\@toodeep\else
    \advance\@itemdepth\@ne
    \edef\@itemitem{labelitem\romannumeral\the\@itemdepth}%
    \expandafter
    \list
      \csname\@itemitem\endcsname
      {\def\makelabel##1{\mbox{##1}\hfil}}%{\hss\llap{##1}}}%
  \fi}

\renewcommand{\labelitemi}{\bfseries --}
\renewcommand{\labelitemii}{$\cdot$}

%% ------ special sizes, for iasart
\providecommand{\titlesize}{}
\providecommand{\authorsize}{}
\providecommand{\abstractsize}{}
\providecommand{\sectionsize}{}
\providecommand{\subsectionsize}{}
\providecommand{\subsubsectionsize}{}

\if@submit
    \renewcommand{\abstractsize}  {\normalsize}
    \renewcommand{\authorsize}    {\large}
    \renewcommand{\titlesize}     {\LARGE}
    \renewcommand{\sectionsize}   {\Large}
    \renewcommand{\subsectionsize}{\large}
    \renewcommand{\subsubsectionsize}{\normalsize}
\else %% final, draft
  \renewcommand{\abstractsize}  {\@setfontsize\abstractsize{10pt}{11pt}}
  \renewcommand{\authorsize}    {\@setfontsize\authorsize{13pt}{18pt}}
  \renewcommand{\titlesize}     {\@setfontsize\titlesize{15pt}{24pt}}
  \renewcommand{\sectionsize}   {\@setfontsize\sectionsize{14pt}{17pt}}
  \renewcommand{\subsectionsize}{\@setfontsize\subsectionsize{12pt}{16pt}}
  \renewcommand{\subsubsectionsize}{\@setfontsize\subsubsectionsize{12pt}{16pt}}
%  \DeclareMathSizes{14}{15.5}{10.95}{8}
%  \DeclareMathSizes{15}{17.28}{12}{10}
%  \DeclareMathSizes{13}{14.4}{10.95}{8}
\fi

%%% --------- paragraphing

\setlength\lineskip{1pt}
\setlength\normallineskip{1pt}

\if@final
  \renewcommand\baselinestretch{1}
  \setlength\parskip{6pt \@plus 2pt \@minus 1pt}
  \setlength\parindent{6mm}
\else
  \if@submit
      \renewcommand\baselinestretch{1.67} %% double spacing
      \setlength\parskip{9pt \@plus 3pt \@minus 2pt}
      \setlength\parindent{1.5em}
   \else
      \renewcommand\baselinestretch{}
      \setlength\parskip{0pt \@plus 1pt}
      \setlength\parindent{17pt}
   \fi
\fi

\if@final
%% probably need to change these:
   \@lowpenalty   51
   \@medpenalty  151
   \@highpenalty 301

% default penalties:
% \clubpenalty150
% \widowpenalty150
%
% \displaywidowpenalty 50
% \predisplaypenalty   10000
% \postdisplaypenalty  0
%
% \interlinepenalty 0
% \brokenpenalty 100
%
   \predisplaypenalty 150 %two column mode: often very difficult to break columns!
\fi


%% --------- Page Layout -----------
%
%    All margin dimensions are measured from a point one inch from the
%    top and lefthand side of the page.
%% ---- vertical spacing and dimension of text ----

\if@final
  \special{papersize=210mm,297mm} %for dvips: in order to produce A4 output
  \setlength\@tempdima{\paperwidth}
  \addtolength\@tempdima{-36mm} % 20 and 16 mm margins
  \setlength\textwidth{\@tempdima}

  \setlength\@tempdima{\paperheight}
  \addtolength\@tempdima{-55mm} % 30 and 25 mm margins
    \addtolength\@tempdima{2pt} % adjusted by eye
  \setlength\textheight{\@tempdima}
%  \addtolength{\voffset}{-16mm}%
  \setlength\topskip   {11pt}
  \setlength\footskip  {12.6mm}

  \setlength\headheight{10pt}
%   \addtolength\footskip{-11pt}
  \setlength\headsep   {15mm}
  \addtolength\headsep {-10pt}% should be height of head line, however adjusted by eye

  \setlength\topmargin{15mm}
  \advance\topmargin by -1in
  \advance\topmargin by -2pt

  \setlength\columnsep{6mm}
  \setlength\columnseprule {0pt}

  \setlength\oddsidemargin{20 mm}
  \advance \oddsidemargin by -1in
  \setlength\evensidemargin{16 mm}
  \advance \evensidemargin by -1in
%
\else
  \setlength\@tempdima{\paperwidth}
  \addtolength\@tempdima{-60mm} % 3cm margins
  \setlength\textwidth{\@tempdima}
  \if@submit \setlength\topskip   {26pt}
  \else \setlength\topskip   {12pt} \fi
  \setlength\headheight{12pt}
  \setlength\footskip  {15mm}
  \addtolength\footskip{-12pt}
  \setlength\headsep   {20pt}

  \setlength\oddsidemargin{30 mm}
  \advance \oddsidemargin by -1in
  \evensidemargin=\oddsidemargin
  \setlength\marginparwidth   {.5\@tempdima}
  \addtolength\marginparwidth {-\marginparsep}
  \addtolength\marginparwidth {-0.4in}

  \setlength\@tempdima{\paperheight}
  \addtolength\@tempdima{-55mm} % 30 and 25 mm margins
  \setlength\textheight{\@tempdima}

  \setlength\topmargin{\paperheight}
  \addtolength\topmargin{-2in}
  \addtolength\topmargin{-\headheight}
  \addtolength\topmargin{-\headsep}
  \addtolength\topmargin{-\textheight}
  \addtolength\topmargin{-\footskip}     % this might be wrong!
  \addtolength\topmargin{-.5\topmargin}
\fi

\@settopoint\textwidth
\@settopoint\textheight
\@settopoint\evensidemargin
\@settopoint\oddsidemargin
\@settopoint\topmargin
\@settopoint\topskip
\@settopoint\headheight
\@settopoint\headsep
\@settopoint\footskip

\setlength\maxdepth{.5\topskip}

%% ---------- width of thin horizontal lines -----

\setlength\arrayrulewidth{0.5pt} %% original: 0.4pt

%% ---------- Makros for users ---------------------
\def\thickhline{%
  \noalign{\ifnum0=`}\fi\hrule \@height 1pt \futurelet
   \reserved@a\@xhline}

%% ---------- Placement of floats ---------------------
\renewcommand\floatpagefraction{.7}   % changed vs article.cls
\renewcommand\dblfloatpagefraction{.7} % changed vs article.cls
\renewcommand\topfraction{1.0}
\renewcommand\dbltopfraction{1.0}
\renewcommand\bottomfraction{1.0}

%% --------- Page style --------------

\def\ps@iasheadings{%
   \if@final
       \def\@oddfoot{\reset@font\hfil\thepage\hfil}
   \else
       \def\@oddfoot{\reset@font\hfil\thepage/\protect\pageref{LastPage}\hfil}
   \fi
       \let\@evenfoot\@oddfoot
       \def\@evenhead{\hfil\slshape\leftmark}%
       \def\@oddhead{{\slshape\rightmark\hfil}}%
       \let\@mkboth\@gobbletwo
       \let\sectionmark\@gobble
       \let\subsectionmark\@gobble
       \let\subsubsectionmark\@gobble
       }

\pagestyle{iasheadings}

\if@final
  \markboth
    %{\vspace*{13mm}\small\scshape\protect\ShortAuthors: \textit{\protect\ShortTitle}}
    {\small\scshape\protect\TheShortAuthors: \textit{\protect\TheShortTitle}}
    {\protect\textnormal{\vspace*{13mm}\small Image Anal Stereol
    \protect\TheVolume:\protect\TheStartPage-\protect\pageref{LastPage}}}
\else
  \if@submit
     \markright{\SubmitPageHead}
  \else
      \markright{{\protect\TheShortTitle}\hfill\today}
   \fi
\fi

%% ------------- Document Markup -------------

%% ----------------   sections and subsections

\renewcommand{\@seccntformat}[1]{}%{\csname the##1\endcsname.}\hspace{0.45em}}
%%% NO NUMBERING %%%
\if@final
\renewcommand\section{\@startsection{section}
        {1}%the level
        {\parindent}% the indent
        {18pt \@plus 2pt \@minus 1pt}%{+parskip=6pt+2-1!
        {6pt \@plus 2pt \@minus 1pt} %
        {\sectionsize\sffamily\MakeTextUppercase}}
\renewcommand\subsection{\@startsection{subsection}
        {2}%the level
        {\parindent}% the indent
        {6pt \@plus 2pt \@minus 1pt}%the beforeskip
        {0.1pt}%the afterskip
        {\subsectionsize\sffamily\MakeTextUppercase}}% the style
\renewcommand\subsubsection{\@startsection{subsubsection}{3}{\parindent}%
        {0.1pt}%
        {0.1pt}%
        {\subsectionsize\sffamily}}
\renewcommand\paragraph{\@startsection{paragraph}{4}{\parindent}%
        {0.1pt}%
        {-1em}%
        {\normalfont\normalsize\itshape}}
\fi
\if@submit
\renewcommand\section{\@startsection{section}
                                     {1}%the level
                                     {\parindent}% the indent
                                     {18pt \@plus 2pt \@minus 1pt}%{+parskip=6pt+2-1!
                                     {6pt \@plus 2pt \@minus 1pt} %
                                     {\sectionsize\normalfont\MakeTextUppercase}}
\renewcommand\subsection{\@startsection{subsection}
                                     {2}%the level
                                     {\parindent}% the indent
                                     {6pt \@plus 2pt \@minus 1pt}%the beforeskip
                                     {0.1pt}%the afterskip
                                     {\subsectionsize\normalfont\MakeTextUppercase}}% the style
\renewcommand\subsubsection{\@startsection{subsubsection}{3}{\parindent}%
                                     {0.1pt}%
                                     {0.1pt}%
                                     {\normalfont\normalsize\MakeTextUppercase}}
\renewcommand\paragraph{\@startsection{paragraph}{4}{\parindent}%
                                    {0.1pt}%
                                    {-1em}%
                                    {\normalfont\normalsize\itshape}}
\fi
%\renewcommand\section{\@startsection {section}{1}{\z@}%
%                                   {-3.5ex \@plus -1ex \@minus -.2ex}%
%                                   {2.3ex \@plus.2ex}%
%                                   {\normalfont\Large\bfseries}}
%\renewcommand\subsection{\@startsection{subsection}{2}{\z@}%
%                                     {-3.25ex\@plus -1ex \@minus -.2ex}%
%                                     {1.5ex \@plus .2ex}%
%                                     {\normalfont\large\bfseries}}
%\renewcommand\subsubsection{\@startsection{subsubsection}{3}{\z@}%
%                                     {-3.25ex\@plus -1ex \@minus -.2ex}%
%                                     {1.5ex \@plus .2ex}%
%                                     {\normalfont\normalsize\bfseries}}
%\renewcommand\paragraph{\@startsection{paragraph}{4}{\z@}%
%                                    {3.25ex \@plus1ex \@minus.2ex}%
%                                    {-1em}%
%                                    {\normalfont\normalsize\bfseries}}
%\renewcommand\subparagraph{\@startsection{subparagraph}{5}{\parindent}%
%                                       {3.25ex \@plus1ex \@minus .2ex}%
%                                       {-1em}%
%                                      {\normalfont\normalsize\bfseries}}

%% ------ Bibliography -------------

\renewcommand\NAT@yrsep{;} % natbib separator between years of same author. default: ,
%\bibliographystyle{ias085}
\bibliographystyle{iasart}

\renewcommand{\bibsection}{\subsection*{References}}
%\if@final\newcommand{\bibfont}{\small}\fi
\setlength{\bibhang}{\parindent}
\setlength{\bibsep}{3pt}


%%% ------------- Title ------------------------


%% ---- Printing Document Info -------------------


%
% looping through authors and affils

\newcount\zaehl

\newcount\auth@rmone

\newcommand{\@lastauthordelim}{\ and}

\newcommand{\@authors}
{ \auth@rmone = \auth@rno
  \advance\auth@rmone by -1\relax
  \ifnum\auth@rno<1{No Author? \PleaseCom{Author}}\fi%no
  \ifnum\value{affil@no}<2\renewcommand{\@affilstar}[1]{\relax}\fi%
  \zaehl=1
  \loop \PrintAuthor{\number\zaehl}\@affilstar{\PrintAuthorsAffil{\number\zaehl}}%
  \ifnum \zaehl< \auth@rno
  {\ifnum \zaehl< \auth@rmone , \else\@lastauthordelim\fi}\ \advance\zaehl by 1\relax %
  \repeat
}

\if@final
\renewcommand{\@lastauthordelim}{\authorsize\scshape\ and}
\fi

\newcommand\@affilsep{}
\if@final\renewcommand\@affilsep{,}\fi
\if@submit\renewcommand\@affilsep{\vspace{0.5em}\newline}\fi
\if@draft\renewcommand\@affilsep{\newline}\fi

\newcommand{\@affiliations}
{\ifnum\value{affil@no}<1{\PleaseCom{Affiliation}}%no
\fi
\ifnum\value{affil@no}<2\renewcommand{\@affilstar}[1]{\relax}\fi%
\zaehl=1
\loop \@affilstar{\number\zaehl}\PrintAffiliation{\number\zaehl}%
\ifnum \zaehl< \value{affil@no}\@affilsep \ \advance\zaehl by 1\relax%
%\ifnum \zaehl< \value{affil@no}; \ \else . \fi %
\repeat
}
%%% some lengths

\newlength\authorindent % indentation of authors / addresses block
\authorindent 0pt
\newlength\authorwidth %
\setlength{\authorwidth}{\textwidth}
\addtolength{\authorwidth}{-\authorindent}

\newlength\abstractindent
\if@final \abstractindent10mm \fi
\newlength\abstractwidth
\setlength{\abstractwidth}{\textwidth}
\addtolength{\abstractwidth}{-2\abstractindent}

\if@submit
  \renewcommand\maketitle{%
  \hyphenpenalty = 10000
  \thispagestyle{empty}
  \begin{Vcenter}
  \begingroup
    \renewcommand\baselinestretch{1.1} %% double spacing
   \begin{center}
   {\titlesize
   %\bfseries%
   \TheTitle}\\
   \par\vskip 3em\noindent
   \Large by\\[+1em]
   \@authors
   \end{center}
   \large
   \par\vskip 1.5em\noindent
   \@affiliations
   \par\vskip 1.5em\noindent
   \ifx\TheEmail\@empty\else{E-mail: \TheEmail}\fi
   \par\vskip3em\noindent
   \begin{tabular}{@{}l p{9cm}}
   Abbreviated running title: &\TheShortTitle\\[+1em]
   Abbreviated authors: &\TheShortAuthors\\[+1em]
   Key words: &\TheKeyWords
   \end{tabular}
  \endgroup
  \end{Vcenter}
  \global\let\maketitle\relax
  \global\let\@author\@empty
  \global\let\@date\@empty
  \global\let\TheTitle\@empty
  \global\let\title\relax
  \global\let\author\relax
  \global\let\date\relax
  \global\let\and\relax
  \renewcommand\baselinestretch{1.67} %% double spacing
  \newpage
  \setcounter{page}{1}
  \hyphenpenalty = 9999
  \section*{Abstract}
   \TheAbstract
  \newpage
  }
\else
  \renewcommand\maketitle{%
  \thispagestyle{plain}
  \begingroup
        \@maketitle
  \endgroup
  \global\let\maketitle\relax
  \global\let\@author\@empty
  \global\let\@date\@empty
  \global\let\TheTitle\@empty
  \global\let\title\relax
  \global\let\author\relax
  \global\let\date\relax
  \global\let\and\relax
  }
\fi

\renewcommand\@maketitle{%
   \if@final%
    \if@ias
     {\vspace*{-22.8mm}
     \small\noindent %%%% Image Anal Stereol %%%% 
     \TheVolume:\TheStartPage-\pageref{LastPage}\\[-0.7mm]
     {\textit\ArticleType}}
     \par
     \else 
      {\vspace*{-22.8mm}
       \small\noindent %%%% Submitted to Image Anal Stereol,
        \pageref{LastPage} pages\\[-0.7mm]
       {\textit\ArticleType}}
     \par
     \fi
   \fi
  {\vspace{5.5mm}
   \raggedright
   \titlesize
   \bfseries\if@final\sffamily\fi%
   \TheTitle}
   \\[16pt\@minus 2pt]
   \noindent\hspace*\authorindent
   \begin{minipage}{\authorwidth}
      \raggedright
      \@authors
      \\[6pt\@minus 2pt]
      \@affiliations
      \if@final
         \ifx\TheEmail\@empty\else{\par\noindent{e-mail: \TheEmail}}\fi
         \if@ias
           \par{\noindent\textit{(Accepted \TheAccepted)}}
         \else
           \par{\noindent\textit{(Submitted)}}
          \fi 
      \else
         \par{\noindent\textit{(Draft from \today)}}
        \fi
   \end{minipage}
   \\[21pt\@minus 2pt]
   \hspace*\abstractindent
   \begin{minipage}[t]{\abstractwidth}%
      \abstractsize
      {\if@final\sffamily\fi ABSTRACT}\\[8pt\@minus 2pt]
      \TheAbstract
      \\[8pt\@minus 2pt]
      \KeyWordsLabel\TheKeyWords.
    \end{minipage}\hfil\\
    \vspace{3mm}
}

%%----- for citations

%\bibpunct{(}{)}{;}{a}{,}{;}

\endinput
%%
%% End of file `iasart.cls'.
